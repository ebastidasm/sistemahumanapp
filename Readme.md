# Manual Técnico


## Introducción

SistemaHumanApp es un sistema profesional contable, que cuenta con modulo de facturación electrónica.
Forma parte del proyecto de Titulación del alumno Eddy Bastidas.

todas las herramientas utilizadas en el desarrollo son OpenSource, en el caso de la utilización de herramientas de pago se utilizaron versiones TRIAL o periodos de prueba.

Se requiere conocimientos previos de Python, Django, Desarrollo web, Deploy.

## Herramientas

| Nombre                   | Instalador                                                                                                                                                                                                                                           |
|:-------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| 
| `Compilador`             | [Python3](https://www.python.org/downloads/release/python-396/ "Python3")                                                                                                                                                                            |
| `IDE de programación`    | [Visual Studio Code](https://code.visualstudio.com/ "Visual Studio Code"), [Sublime Text](https://www.sublimetext.com/ "Sublime Text"), [Pycharm](https://www.jetbrains.com/es-es/pycharm/download/#section=windows "Pycharm")                       |
| `Motor de base de datos` | [Sqlite Studio](https://github.com/pawelsalawa/sqlitestudio/releases "Sqlite Studio"), [PostgreSQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads "PostgreSQL"), [MySQL](https://www.apachefriends.org/es/index.html "MySQL") |

## Pasos para instalación

##### 1) Descargar e Instalar VisualStudioCode

```bash
https://code.visualstudio.com/docs/?dv=win64user
```

##### 2)Descomprimir el proyecto en una carptera del sistema operativo.


##### 3) Instalar Java en el computador, esto es muy importante para poder firmar los comprobantes con la firma electrónica 

```bash
https://www.java.com/es/download/
```

##### 4) Crear un entorno virtual para posteriormente instalar las librerias del proyecto

```bash
python -m venv venv
```

##### 5) Instalar el complemento de [weasyprint](https://weasyprint.org/ "weasyprint")

Para Windows debe descargar el complemento de [GTK3 installer](https://github.com/tschoonj/GTK-for-Windows-Runtime-Environment-Installer/releases "GTK3 installer"). En algunas ocaciones se debe colocar en las variables de entorno al inicio del listado para que funcione y adicionalmente se debe reiniciar el computador.

##### 6) Activar el entorno virtual de nuestro proyecto

```bash
cd venv\Scripts\activate.bat 
```

##### 7) Instalar todas las librerias del proyecto que se encuentran en la carpeta deploy

```bash
pip install -r deploy/txt/requirements.txt
```

##### 8) Crear la tablas de la base de datos a partir de las migraciones

```bash
python manage.py makemigrations
python manage.py migrate
```

##### 9) Insertar datos en las entidades de los modulos de seguridad y usuario del sistema

```bash
python manage.py start_installation
```

##### 10) Insertar datos iniciales de categorías, productos, clientes, compras y ventas (este paso es opcional)

```bash
python manage.py insert_test_data
```

##### 11) Iniciar el servidor del proyecto

```bash
python manage.py runserver 
```

Si se desea verlo en toda la red se debe ejecutar de la siguiente manera:

```bash
python manage.py runserver 0:8000 o python manage.py runserver 0.0.0.0:8000
```

##### 12) Iniciar sesión en el sistema (Puede cambiar la clave y usuario que se crea en el archivo core/management/commands/start_installation.py del paso 8)

```bash
username: admin
password: hacker94
```

# Pasos para la creación del cron de envió de comprobantes electrónicos

##### 1) Instarlar cron en el servidor de linux

```bash
sudo apt install cron
```

##### 2) Crear una nueva tarea en cron

```bash
crontab -e
```

##### 3) Crear la tarea de envio de correos en cron, la palabra user hace referencia al usuario del server

```bash
*/1 * * * * bash /home/user/invoice/deploy/sh/electronic_billing.sh
```

##### 4) Reiniciar el servicio de cron en el servidor

```bash
sudo /etc/init.d/cron restart
```
